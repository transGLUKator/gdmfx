(function($){
  'use strict';

  $(document).ready(function () {
    // Toggle expanded class on global page tabs
    $('.global-tabs > .menu').children('li').on('mouseover', function () {
      $(this).siblings().removeClass('expanded');
      $(this).addClass('expanded');
    });

    // Get the height of highest tab and set margin on .global-page accordingly
    function maxTabsHeight(){
      var maxTabHeight = 0;
      $('.global-tabs > .menu > li').children('.menu').each(function () {
        var tabHeight = $(this).height();

        maxTabHeight = maxTabHeight < tabHeight ? tabHeight : maxTabHeight;
      });

      $('.global-tabs').css('padding-bottom', maxTabHeight + 30 + 'px');
    }

    maxTabsHeight();

  });
})(jQuery);
